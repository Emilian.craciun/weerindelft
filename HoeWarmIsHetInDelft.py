from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import re
import os


chrome_options=webdriver.ChromeOptions()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("window-size=1400,2100") 
chrome_options.add_argument('--disable-gpu')

driver=webdriver.Chrome(os.getenv('CHROMEDRIVER_DIR'),chrome_options=chrome_options)

driver.get('https://www.weerindelft.nl/')
found = False
try:
    driver.switch_to.frame(driver.find_element_by_name("ifrm_3"))
    found_element = driver.find_element_by_id('ajaxthermometer')
    element_content = found_element.get_attribute('innerHTML')
    found = True
except NoSuchElementException:
    print('Unable to locate element..')

if found:

    patt =  r"Currently:\s+(\S+)°"
    print(f"{round(float(re.search(patt, element_content)[1]))} degress Celsius")


driver.close()


